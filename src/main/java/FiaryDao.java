import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;

public class FiaryDao {
    public Fairy persist(Fairy fairy) {
        EntityManager em = FairyEMF.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.persist(fairy);
        tx.commit();
        em.close();
        return fairy;
    }

    public List<Fairy> findAllCAPI(){
        EntityManager em = FairyEMF.createEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Fairy> query = cb.createQuery(Fairy.class);
        query.from(Fairy.class);
        List<Fairy> fairies = em.createQuery(query).getResultList();
        em.close();
        return fairies;
    }

    public List<Fairy> findAll(){
        EntityManager em = FairyEMF.createEntityManager();
        List<Fairy> fairies = em.createQuery("select m from Fairy m").getResultList();
        em.close();
        return fairies;
    }

}
