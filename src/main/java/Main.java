import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args){

        FiaryDao dao = new FiaryDao();

        List<Fairy> fairies = new ArrayList<>();
        fairies.add(new Fairy(30,50,"Trist"));
        fairies.add(new Fairy(32,13,"Root"));
        fairies.add(new Fairy(12,3,"Pulp"));
        fairies.add(new Fairy(24,19,"Dixy"));
        fairies.add(new Fairy(19,43,"Cloy"));

        for(Fairy i : fairies){
            dao.persist(i);
        }

        Fairy fairy = new Fairy(40,40, "TestFairy");
        EntityManager em = FairyEMF.createEntityManager();

        EntityTransaction transaction = em.getTransaction();

        transaction.begin();
        em.persist(fairy);
        transaction.commit();

        System.out.println("TestFairy added");
        em.close();
        System.out.println("Test EntityManager closed");
        fairy.setId(40);
        System.out.println("TestFairy Id set to 40");

        System.out.println("printing fairies");
        dao.findAllCAPI().forEach(System.out::println);
        System.out.println();
        System.out.println("printing fairies HARDCODDED");
        dao.findAll().forEach(System.out::println);

        FairyEMF.close();

        System.out.println("All done");


    }
}
