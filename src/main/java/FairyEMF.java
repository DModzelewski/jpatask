import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class FairyEMF {
    private static final EntityManagerFactory emf =
            Persistence.createEntityManagerFactory("FairysPU");

    public static final EntityManager createEntityManager() {
        return emf.createEntityManager();
    }

    public static final void close(){
        emf.close();
    }
}
